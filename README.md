# Blogo [![Go Report Card](https://goreportcard.com/badge/github.com/arnaucode/blogo)](https://goreportcard.com/report/github.com/arnaucode/blogo)
Static blog generator, templating engine from markdown and html templates

![blogo](https://raw.githubusercontent.com/arnaucube/blogo/master/blogo.png "blogo")

## Use
A complete usage example can be found in this repo: https://github.com/arnaucube/blogoExample


---

Blogo is used in https://arnaucode.com/blog
